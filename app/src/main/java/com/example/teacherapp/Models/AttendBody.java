package com.example.teacherapp.Models;

import com.google.gson.annotations.SerializedName;

public class AttendBody {
    @SerializedName("id")
    private String id;
    @SerializedName("status")
    private String status;

    public AttendBody(String id, String status) {
        this.id = id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
