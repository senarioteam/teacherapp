package com.example.teacherapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;
    @SerializedName("auth_token")
    @Expose
    private String authToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("childs_name")
    @Expose
    private String childsName;
    @SerializedName("childs_age")
    @Expose
    private String childsAge;
    @SerializedName("childs_gender")
    @Expose
    private String childsGender;
    @SerializedName("childs_grade")
    @Expose
    private String childsGrade;
    @SerializedName("Schools_name")
    @Expose
    private String schoolsName;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("pickup_lat")
    @Expose
    private String pickupLat;
    @SerializedName("pickup_lng")
    @Expose
    private String pickupLng;
    @SerializedName("dropoff_lat")
    @Expose
    private String dropoffLat;
    @SerializedName("dropoff_lng")
    @Expose
    private String dropoffLng;
    @SerializedName("payment_status")
    @Expose
    private Integer paymentStatus;
    @SerializedName("block_status")
    @Expose
    private Integer blockStatus;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("childs_image")
    @Expose
    private String childsImage;
    @SerializedName("driver_lat")
    @Expose
    private Double driverLat;
    @SerializedName("driver_long")
    @Expose
    private Double driverLong;
    @SerializedName("childs_attendance")
    @Expose
    private String childsAttendance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getChildsName() {
        return childsName;
    }

    public void setChildsName(String childsName) {
        this.childsName = childsName;
    }

    public String getChildsAge() {
        return childsAge;
    }

    public void setChildsAge(String childsAge) {
        this.childsAge = childsAge;
    }

    public String getChildsGender() {
        return childsGender;
    }

    public void setChildsGender(String childsGender) {
        this.childsGender = childsGender;
    }

    public String getChildsGrade() {
        return childsGrade;
    }

    public void setChildsGrade(String childsGrade) {
        this.childsGrade = childsGrade;
    }

    public String getSchoolsName() {
        return schoolsName;
    }

    public void setSchoolsName(String schoolsName) {
        this.schoolsName = schoolsName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getPickupLng() {
        return pickupLng;
    }

    public void setPickupLng(String pickupLng) {
        this.pickupLng = pickupLng;
    }

    public String getDropoffLat() {
        return dropoffLat;
    }

    public void setDropoffLat(String dropoffLat) {
        this.dropoffLat = dropoffLat;
    }

    public String getDropoffLng() {
        return dropoffLng;
    }

    public void setDropoffLng(String dropoffLng) {
        this.dropoffLng = dropoffLng;
    }

    public Integer getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Integer paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getBlockStatus() {
        return blockStatus;
    }

    public void setBlockStatus(Integer blockStatus) {
        this.blockStatus = blockStatus;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getChildsImage() {
        return childsImage;
    }

    public void setChildsImage(String childsImage) {
        this.childsImage = childsImage;
    }

    public Double getDriverLat() {
        return driverLat;
    }

    public void setDriverLat(Double driverLat) {
        this.driverLat = driverLat;
    }

    public Double getDriverLong() {
        return driverLong;
    }

    public void setDriverLong(Double driverLong) {
        this.driverLong = driverLong;
    }

    public String getChildsAttendance() {
        return childsAttendance;
    }

    public void setChildsAttendance(String childsAttendance) {
        this.childsAttendance = childsAttendance;
    }

}