package com.example.teacherapp.Models;

import com.google.gson.annotations.SerializedName;

public class AuthModel {
    @SerializedName("message")
    private String message;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("phone_no")
    private String phone_no;
    @SerializedName("user_type")
    private String user_type;

    public AuthModel(String name, String email, String password, String phone_no, String user_type) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone_no = phone_no;
        this.user_type = user_type;
    }

    public AuthModel(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }
}
