package com.example.teacherapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.teacherapp.ApiPost;
import com.example.teacherapp.Loading;
import com.example.teacherapp.Models.AuthModel;
import com.example.teacherapp.R;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignupActivity extends AppCompatActivity {

    private TextInputLayout name, email, password, phone;
    private Button signup;
    public static String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getSupportActionBar().hide();

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.pass);
        phone = findViewById(R.id.phone);
        signup = findViewById(R.id.signupbutton);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nametext = name.getEditText().getText().toString();
                String emailtext = email.getEditText().getText().toString();
                String passwordtext = password.getEditText().getText().toString();
                String phonetext = phone.getEditText().getText().toString();

                if (TextUtils.isEmpty(nametext)) {
                    name.setError("enter your name");
                    return;
                } else {
                    name.setError(null);
                }
                if (TextUtils.isEmpty(emailtext)) {
                    email.setError("enter your email");
                    return;
                } else {
                    email.setError(null);
                }
                if (TextUtils.isEmpty(passwordtext)) {
                    password.setError("enter your password");
                    return;
                } else {
                    password.setError(null);
                }
                if (TextUtils.isEmpty(phonetext)) {
                    phone.setError("enter phone number");
                    return;
                } else {
                    phone.setError(null);
                }
                if (passwordtext.length() < 8) {
                    password.setError("password too short");
                    return;
                } else {
                    password.setError(null);
                }
                if (!emailtext.matches(emailpattern)) {
                    email.setError("invalid email");
                    return;
                } else {
                    email.setError(null);
                }
                signupUser(nametext, emailtext,passwordtext, phonetext,"teacher");
            }
        });
    }

    private void signupUser(String name, String email, String password, String phone_no, String user_type) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        AuthModel authModel = new AuthModel(name, email, password, phone_no, user_type);
        Call<AuthModel> call = apiPost.createUser(authModel);
        call.enqueue(new Callback<AuthModel>() {
            @Override
            public void onResponse(Call<AuthModel> call, Response<AuthModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Loading.dismiss();
                        Toast.makeText(SignupActivity.this, "Signed up Successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                        finish();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(SignupActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AuthModel> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(SignupActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void moveToLogin(View view) {
        startActivity(new Intent(SignupActivity.this, LoginActivity.class));
        finish();
    }
}