package com.example.teacherapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.teacherapp.ApiPost;
import com.example.teacherapp.Loading;
import com.example.teacherapp.Models.AuthModel;
import com.example.teacherapp.Models.AuthSession;
import com.example.teacherapp.Models.UserSession;
import com.example.teacherapp.R;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.teacherapp.Activities.SignupActivity.emailpattern;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout email, password;
    private Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        email = findViewById(R.id.loginemail);
        password = findViewById(R.id.loginpassword);
        btnlogin = findViewById(R.id.loginbutton);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailtext = email.getEditText().getText().toString();
                String passwordtext = password.getEditText().getText().toString();

                if (TextUtils.isEmpty(emailtext)) {
                    email.setError("enter your email");
                    return;
                } else {
                    email.setError(null);
                }
                if (TextUtils.isEmpty(passwordtext)) {
                    password.setError("enter your password");
                    return;
                } else {
                    password.setError(null);
                }
                if (passwordtext.length() < 8) {
                    password.setError("password too short");
                    return;
                } else {
                    password.setError(null);
                }
                if (!emailtext.matches(emailpattern)) {
                    email.setError("invalid email");
                    return;
                } else {
                    email.setError(null);
                }
                UserSession user = new UserSession(emailtext, passwordtext);
                AuthSession authSession = new AuthSession(LoginActivity.this);
                authSession.saveSession(user);
                loginUser(emailtext, passwordtext);
            }
        });
    }

    private void loginUser(String email, String password) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        AuthModel postModel = new AuthModel(email, password);
        Call<AuthModel> call = apiPost.loginUser(postModel);
        call.enqueue(new Callback<AuthModel>() {
            @Override
            public void onResponse(Call<AuthModel> call, Response<AuthModel> response) {
                if (response.isSuccessful()) {
                    AuthModel resObj = response.body();
                    if (resObj.getMessage().equals("Signed in successfully")) {
                        Loading.dismiss();
                        Toast.makeText(LoginActivity.this, "Welcome", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(LoginActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AuthModel> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void moveToSignup(View view) {
        startActivity(new Intent(LoginActivity.this, SignupActivity.class));
        finish();
    }
}