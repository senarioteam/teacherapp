package com.example.teacherapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.teacherapp.ApiPost;
import com.example.teacherapp.Loading;
import com.example.teacherapp.Models.ChildRecRes;
import com.example.teacherapp.Models.Datum;
import com.example.teacherapp.R;
import com.example.teacherapp.ShowRecordAdapter;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PresentRecActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private ShowRecordAdapter showRecordAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_present_rec);
        recyclerView = findViewById(R.id.rv_present);
        img = findViewById(R.id.imgNoRecordspr);
        swipeRefreshLayout = findViewById(R.id.present_swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                childRecordList("present");
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        childRecordList("present");
    }
    private void childRecordList(String status) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildRecRes childRecRes = new ChildRecRes(null,status);
        Call<ChildRecRes> call = apiPost.showAttendRec(childRecRes);
        call.enqueue(new Callback<ChildRecRes>() {
            @Override
            public void onResponse(Call<ChildRecRes> call, Response<ChildRecRes> response) {
                if (response.isSuccessful()) {
                    ChildRecRes resObj = response.body();
                    List<Datum> list = resObj.getData();

                    if (list != null) {
                        showRecordAdapter = new ShowRecordAdapter((ArrayList<Datum>) list, PresentRecActivity.this);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(PresentRecActivity.this, RecyclerView.VERTICAL, false);
                        Loading.dismiss();
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(showRecordAdapter);
                        showRecordAdapter.notifyDataSetChanged();
                    } else {
                        Loading.dismiss();
                        img.setVisibility(View.VISIBLE);
                        findViewById(R.id.tvNoRecordspr).setVisibility(View.VISIBLE);
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(PresentRecActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChildRecRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(PresentRecActivity.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(PresentRecActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}