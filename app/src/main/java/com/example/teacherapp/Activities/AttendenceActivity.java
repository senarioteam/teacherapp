package com.example.teacherapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teacherapp.ApiPost;
import com.example.teacherapp.Loading;
import com.example.teacherapp.Models.AttendBody;
import com.example.teacherapp.Models.AttendRes;
import com.example.teacherapp.Models.ChildRecRes;
import com.example.teacherapp.Models.Datum;
import com.example.teacherapp.R;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AttendenceActivity extends AppCompatActivity {
    TextView parentId, childName, schoolName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence);
        parentId = findViewById(R.id.parentid);
        childName = findViewById(R.id.childName);
        schoolName = findViewById(R.id.schoolName);
        String parentId = getIntent().getStringExtra("ParentIdKey");
        showChildRec(parentId);

        findViewById(R.id.btnPresent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAttendance(parentId,"present");
            }
        });
        findViewById(R.id.btnLate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAttendance(parentId,"late");
            }
        });
    }

    private void markAttendance(String id, String status) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        AttendBody attendBody = new AttendBody(id, status);
        Call<AttendRes> call = apiPost.markAttendance(attendBody);
        call.enqueue(new Callback<AttendRes>() {
            @Override
            public void onResponse(Call<AttendRes> call, Response<AttendRes> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Loading.dismiss();
                        Intent intent = new Intent(AttendenceActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(AttendenceActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AttendRes> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Loading.dismiss();
                    Toast.makeText(AttendenceActivity.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(AttendenceActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void showChildRec(String id) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildRecRes childRecRes = new ChildRecRes(id,null);
        Call<ChildRecRes> call = apiPost.showChildRec(childRecRes);
        call.enqueue(new Callback<ChildRecRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ChildRecRes> call, Response<ChildRecRes> response) {
                if (response.isSuccessful()) {
                    ChildRecRes resObj = response.body();
                    List<Datum> list = resObj.getData();
                    Loading.dismiss();
                    childName.setText("Name: "+list.get(0).getChildsName());
                    schoolName.setText("School: "+list.get(0).getSchoolsName());

                } else {
                    Toast.makeText(AttendenceActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChildRecRes> call, Throwable t) {
                Toast.makeText(AttendenceActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}