package com.example.teacherapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teacherapp.Models.Datum;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ShowRecordAdapter extends RecyclerView.Adapter<ShowRecordAdapter.ViewHolder>{
    private ArrayList<Datum> presentList = new ArrayList<>();
    private Context context;

    public ShowRecordAdapter(ArrayList<Datum> datumArrayList, Context context) {
        this.presentList = datumArrayList;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_rec_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

        holder.name.setText(presentList.get(position).getChildsName());
        holder.attendance.setText(presentList.get(position).getChildsAttendance());
        holder.phone.setText(presentList.get(position).getPhoneNo());
        holder.create_time.setText(presentList.get(position).getCreatedAt());

    }

    @Override
    public int getItemCount() {
        return presentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,attendance,phone,create_time;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.child_name);
            attendance = itemView.findViewById(R.id.child_attendance);
            phone = itemView.findViewById(R.id.phone_no);
            create_time = itemView.findViewById(R.id.createdat);

        }
    }
}
