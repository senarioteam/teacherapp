package com.example.teacherapp.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.teacherapp.Activities.AbsentRecActivity;
import com.example.teacherapp.Activities.LateRecActivity;
import com.example.teacherapp.Activities.PresentRecActivity;
import com.example.teacherapp.R;

public class HomeFragment extends Fragment {

    Button btnPresentRec, btnAbsentRec, btnLateRec;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        btnPresentRec = view.findViewById(R.id.btnPresentRec);
        btnAbsentRec = view.findViewById(R.id.btnAbsentRec);
        btnLateRec = view.findViewById(R.id.btnLateRec);
        btnPresentRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PresentRecActivity.class);
                startActivity(intent);
            }
        });
        btnAbsentRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AbsentRecActivity.class);
                startActivity(intent);
            }
        });
        btnLateRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LateRecActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

}