package com.example.teacherapp;

import com.example.teacherapp.Models.AttendBody;
import com.example.teacherapp.Models.AttendRes;
import com.example.teacherapp.Models.AuthModel;
import com.example.teacherapp.Models.ChildRecRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiPost {
    @POST("teacher_sign_up")
    Call<AuthModel> createUser(@Body AuthModel authModel);

    @POST("teacher_sign_in")
    Call<AuthModel> loginUser(@Body AuthModel authModel);

    @POST("attendance")
    Call<AttendRes> markAttendance(@Body AttendBody attendBody);

    @POST("attendance_records")
    Call<ChildRecRes> showAttendRec(@Body ChildRecRes childRecRes);

    @POST("get_data_parent")
    Call<ChildRecRes> showChildRec(@Body ChildRecRes childRecRes);
}
